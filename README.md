# Timestamp-helper
配信視聴時にリアルタイムでタイムスタンプを作成する補助ツールです。(開発途中)

## 必要なもの
- 最新版のNode.js(執筆時はv18.3.0)
- Youtube Data API v3が有効なGCPのAPIキー

## 起動方法
1. `.env`を作成し、APIキーを設定(`.env.example`を参考に)
2. `npm i`
3. `npm run start ${liveId}`

## 使い方
![screenshot](https://gitlab.com/foctanu/timestamp-helper/uploads/8ec536535b4000925ac351a7d3e81672/Screen_Shot_2022-06-16_at_2.09.00.png)

1. `Press Enter to set timestamp` が出ているときにEnterキーを押すと、timestamp打刻
2. `Please Enter a memo` で打刻した時間にメモを記録（文字を入力したあとEnter)
3. メモの入力欄で`abort`と打つと現在保持しているtimestampを破棄。次の打刻に移ります。 
4. メモの入力欄で`exit`と打つと終了。結果が表示されます。 

## 注意点
配信が始まってすぐに使うことを想定しているので、配信前、配信後にプログラムを走らせようとするとエラーが出るようにしています。

MacOSでは、ターミナルで日本語入力すると一文字削除(Delete押下)時に表示がズレます。これはMacOSの問題なので避けようがなさそうです。
