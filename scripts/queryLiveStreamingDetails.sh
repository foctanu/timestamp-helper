#!/bin/zsh
source ../.env

ENDPOINT_ROOT="https://www.googleapis.com/youtube/v3"
ENDPOINT_VIDEOS="/videos"

if [ $# -eq 0 ]; then
  echo "Usage: $0 <liveChatId>"
  exit 1
fi

curl "${ENDPOINT_ROOT}${ENDPOINT_VIDEOS}?part=liveStreamingDetails&id=$1&key=${API_KEY}"
