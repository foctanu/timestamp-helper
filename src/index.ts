import readlineSync from 'readline-sync';
import chalk from 'chalk';
import { liveStreamingDetailsFetcher } from './utils';

interface RawTimeEntry {
  time: Date;
  memo: string;
}

interface TimeEntry extends RawTimeEntry {
  timestamp: string;
  seconds: number;
}

function exit(msg: string) {
  console.error(msg);
  process.exit(1);
}

async function setTimestamp() {
  readlineSync.question('Press Enter to set timestamp.');
  return new Date();
}

async function createRawTimeEntry(time: Date) {
  const memo = readlineSync.question('Please enter a memo: ');

  const timeEntry: RawTimeEntry = {
    time,
    memo,
  };

  return timeEntry;
}

function formatTime(time: Date, actualStartTime: Date): [string, number] {
  const diff = Math.floor((time.getTime() - actualStartTime.getTime()) / 1000);
  const h = Math.floor(diff / 3600);
  const m = Math.floor((diff - h * 3600) / 60);
  const s = Math.floor(diff - h * 3600 - m * 60);

  function pad(n: number): string {
    return n < 10 ? `0${n}` : `${n}`;
  }

  return [`${pad(h)}:${pad(m)}:${pad(s)}`, diff];
}

function formatTimeEntries(timeEntries: RawTimeEntry[], actualStartTime: Date) {
  const formattedTimeEntries: TimeEntry[] = timeEntries.map(({ time, memo }) => {
    const [formattedtime, seconds] = formatTime(time, actualStartTime);
    const result: TimeEntry = {
      timestamp: formattedtime,
      seconds,
      time,
      memo,
    };
    return result;
  });

  return formattedTimeEntries;
}

function generateComment(timeEntries: TimeEntry[]) {
  const lines: string[] = timeEntries.map((timeEntry) => {
    return `${timeEntry.timestamp} ${timeEntry.memo}`;
  });
  const comment = lines.join('\n');
  return comment;
}

async function confirmTimeEntries(liveId: string, timeEntries: TimeEntry[]) {
  // TODO: ask user to confirm time entries and allow them to edit them
  timeEntries.forEach((timeEntry) => {
    const endpoint_base = 'https://youtu.be/';
    const url = `${endpoint_base}${liveId}?t=${timeEntry.seconds}`;
    console.log(chalk.bold(timeEntry.memo), `(${url})`);
  });
}

async function main() {
  const rawTimeEntries: RawTimeEntry[] = [];
  let timeEntries: TimeEntry[];
  let comment: string;
  const liveId: string = process.argv[2] ?? exit('Please provide a live id.');
  const item =
    (await liveStreamingDetailsFetcher(liveId)) ?? exit(`Couldn't find live with id: ${liveId}`);
  const actualStartTime = item.liveStreamingDetails?.actualStartTime;
  const actualEndTime = item.liveStreamingDetails?.actualEndTime;

  if (actualStartTime === null || actualStartTime === undefined) {
    console.error('Live stream is not started yet.');
    console.log(item);
    process.exit(0);
  } else if (actualEndTime !== undefined && actualEndTime !== null) {
    console.error('Live stream is already ended.');
    console.log(item);
    process.exit(0);
  }

  // loop until user enters 'exit'
  loop: while (true) {
    const time = await setTimestamp();
    const rawTimeEntry = await createRawTimeEntry(time);

    switch (rawTimeEntry.memo) {
      case 'exit':
        console.log(chalk.cyan('Exiting...\n'));
        break loop;
      case 'abort':
        console.log(chalk.gray('Abort'));
        continue;
      default:
        rawTimeEntries.push(rawTimeEntry);
    }
  }

  timeEntries = formatTimeEntries(rawTimeEntries, new Date(actualStartTime));

  confirmTimeEntries(liveId, timeEntries);

  console.log('');
  console.log(
    chalk.bold.black.bgGreen('Result'),
    '\n----------------------------------------------------',
  );
  comment = generateComment(timeEntries);
  console.log(comment, '\n----------------------------------------------------');
}

main();
