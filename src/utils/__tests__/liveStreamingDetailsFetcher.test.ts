import liveStreamingDetailsFetcher from '../../utils/liveStreamingDetailsFetcher';

describe('liveStreamingDetailsFetcher', () => {
  it('return', async () => {
    const item = await liveStreamingDetailsFetcher('NqQVjQINBbM');

    expect(item).toEqual({
      kind: 'youtube#video',
      etag: '5ayds98JXj1LWA3GZ4xogBG6hxs',
      id: 'NqQVjQINBbM',
      liveStreamingDetails: {
        actualStartTime: '2022-04-26T11:08:04Z',
        actualEndTime: '2022-04-26T21:17:04Z',
        scheduledStartTime: '2022-04-26T11:00:00Z',
      },
    });
  });
});
