import { youtube_v3 } from 'googleapis';

export default async function fetcher(_id: string) {
  const yt = new youtube_v3.Youtube({ auth: process.env.API_KEY });

  const result = await yt.videos.list({
    part: ['liveStreamingDetails'],
    id: [_id],
  });

  if (result.data.items === undefined) throw new Error('Live stream is not found.');

  return result.data.items[0];
}
